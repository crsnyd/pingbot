<?php
require __DIR__.'/vendor/autoload.php';
$arrSite = array ('google.com');
$date=date('Y-m-d H:i:s');
foreach($arrSite as $target){
    if (pingCheck($target, $date)){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, TRUE);               
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);               
        curl_setopt($ch, CURLOPT_USERAGENT, 'RASPingbot');  
        curl_setopt($ch, CURLOPT_URL, $target);             
        curl_setopt($ch, CURLOPT_VERBOSE, FALSE);           
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);     
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);     
        $return_array['FILE']   = curl_exec($ch); 
        $return_array['STATUS'] = curl_getinfo($ch);
        $http_code=$return_array['STATUS']['http_code'];
        curl_close($ch);
        //fclose($socket);
        $CheckStatus=Http_Code_email($http_code, $target);
        if($CheckStatus){
        }else{
            die("Server is Down ". $date."\n\r");
        }   
    }else 
    {
        echo "Offline! ".$target." ".$date."\n\r";
        $CheckStatus=Http_Code_email('ping_fail', $target);  
    }
}
function pingCheck($target, $date){
    if(!$socket = @fsockopen($target, 80, $errno, $errstr, 30)){
        if(!$socket = @fsockopen('google.com', 80, $errno, $errstr, 30)){
            die("Internet is down! ".$date."\n\r");//
        }else{   
            return false;
        }
    }else{
        return true; 
    }
}
function Mailer($mailto_array, $mailfrom, $SiteName, $message=""){
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Debugoutput = 'html';
    $mail->Host = $_ENV["HOST"]; 
    $mail->SMTPAuth = true;
    $mail->Port = 25;
    $mail->Username = $_ENV["USER"];
    $mail->Password = $_ENV["PASSWORD"]; 
    $mail->From = $mailfrom;
    $mail->FromName = "RASBot";
    foreach($mailto_array as $mailto)
    {
       $mail->addAddress($mailto, "RAS");
    }
    $date=date('Y-m-d H:i:s');
    $mail->Subject = 'URGENT SERVER DOWN: '.$date.' '.$SiteName;
    $mail->Body = ''.$SiteName.'/n';
    if($message!=""){
      $mail->Body = $message;
    }
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
}

function Http_Code_email($http_code, $SiteName){
    #-------------------------------------
    # Define 100 series http codes (informational)
    #-------------------------------------
    $status_code_array[100]  = "100 Continue";
    $status_code_array[101]  = "101 Switching Protocols";

    #-------------------------------------
    # Define 200 series http codes (successful)
    #-------------------------------------
    $status_code_array[200]  = "200 OK";
    $status_code_array[201]  = "201 Created";
    $status_code_array[202]  = "202 Accepted";
    $status_code_array[203]  = "203 Non-Authoritative Information";
    $status_code_array[204]  = "204 No Content";
    $status_code_array[205]  = "205 Reset Content";
    $status_code_array[206]  = "206 Partial Content";

    #-------------------------------------
    # Define 300 series http codes (redirection)
    #-------------------------------------
    $status_code_array[300]  = "300 Multiple Choices";
    $status_code_array[301]  = "301 Moved Permanently";
    $status_code_array[302]  = "302 Found";
    $status_code_array[303]  = "303 See Other";
    $status_code_array[304]  = "304 Not Modified";
    $status_code_array[305]  = "305 Use Proxy";
    $status_code_array[306]  = "306 (Unused)";
    $status_code_array[307]  = "307 Temporary Redirect";

    #-------------------------------------
    # Define 400 series http codes (client error)
    #-------------------------------------
    $status_code_array[400]  = "400 Bad Request";
    $status_code_array[401]  = "401 Unauthorized";
    $status_code_array[402]  = "402 Payment Required";
    $status_code_array[403]  = "403 Forbidden";
    $status_code_array[404]  = "404 Not Found";
    $status_code_array[405]  = "405 Method Not Allowed";
    $status_code_array[406]  = "406 Not Acceptable";
    $status_code_array[407]  = "407 Proxy Authentication Required";
    $status_code_array[408]  = "408 Request Timeout";
    $status_code_array[409]  = "409 Conflict";
    $status_code_array[410]  = "410 Gone";
    $status_code_array[411]  = "411 Length Required";
    $status_code_array[412]  = "412 Precondition Failed";
    $status_code_array[413]  = "413 Request Entity Too Large";
    $status_code_array[414]  = "414 Request-URI Too Long";
    $status_code_array[415]  = "415 Unsupported Media Type";
    $status_code_array[416]  = "416 Requested Range Not Satisfiable";
    $status_code_array[417]  = "417 Expectation Failed";

    #-------------------------------------
    # Define 500 series http codes (server error)
    #-------------------------------------
    $status_code_array[500]  = "500 Internal Server Error";
    $status_code_array[501]  = "501 Not Implemented";
    $status_code_array[502]  = "502 Bad Gateway";
    $status_code_array[503]  = "503 Service Unavailable";
    $status_code_array[504]  = "504 Gateway Timeout";
    $status_code_array[505]  = "505 HTTP Version Not Supported";
    
    $status_code_array['ping_fail'] = "The ping to the web has failed";
    if( 200 <= $http_code && $http_code <= 307){
        return true;
    }else{
        $message = $status_code_array[$http_code]." -> $SiteName ";
        Mailer($_ENV["TO"], $_ENV["FROM"] , $SiteName, $message);
        return false; 
    }
}
?>
